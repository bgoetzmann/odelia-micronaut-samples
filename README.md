# odelia-micronaut-samples

This is the repository for Micronaut framework experimentations in Kotlin language; we have for now this project:

- declarative-http-client, see the accompaging post [Exemple d'un client HTTP déclaratif de Micronaut en langage Kotlin](https://www.odelia-technologies.com/blog/mn-declarative-http-client.html).