package odelia.micronaut.samples.httpclient

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        val applicationCtx = Micronaut.build()
                .packages("odelia.micronaut.samples.httpclient")
                .mainClass(Application.javaClass)
                .start()

        val client = applicationCtx.createBean(SwApiClient::class.java)
        // Get Luke Skywalker character using Map type
        getCharacterUsingMap(client, "1")
        // Get Luke Skywalker character
        getCharacter(client, "1")
        // Fetch all people
        getPeople(client)
    }

    fun getCharacterUsingMap(client: SwApiClient, id: String) {
        client.getCharacterUsingMap(id)
                .subscribe { m -> println(m) }
    }

    fun getCharacter(client: SwApiClient, id: String) {
        client.getCharacter(id)
                .subscribe { c -> println(c) }
    }

    fun getPeople(client: SwApiClient) {
        val people = mutableListOf<Character>()
        var page = 1
        var next = true
        while (next) {
            val peopleResponse = client.getPeople(page++).blockingGet()
            people.addAll(peopleResponse.results!!)
            next = peopleResponse.next != null
        }

        println("Number of people: ${people.size}")
        println(people.filter { it.gender == "male" }.joinToString { it.name })
    }
}