package odelia.micronaut.samples.httpclient

import io.micronaut.http.annotation.Get
import io.micronaut.http.client.Client
import io.reactivex.Single

data class Character(var name: String = "", var gender: String = "", var birth_year: String = "")

data class PeopleResponse(var count: Int = 0, var next: String? = "", var results: List<Character>? = null)

@Client("https://swapi.co/api/")
interface SwApiClient {

    @Get("/people/{id}")
    fun getCharacterUsingMap(id: String): Single<Map<String,*>>

    @Get("/people/{id}")
    fun getCharacter(id: String): Single<Character>

    @Get("/people/?page={pageNumber}")
    fun getPeople(pageNumber: Int = 1): Single<PeopleResponse>
}